// invoke function directly
require('./weatherReader.js')('Maryville, Missouri')

// invoke named function
const weatherReader = require('./weatherReader.js')
weatherReader('Paris, France')

// invoke object with multiple functions
const weatherServices = require('./weatherServices.js')
weatherServices.getTemp('Hyderabad, India')
weatherServices.getHumidity('Hyderabad , India')

// in what order will our functions return?
