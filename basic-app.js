const http = require("http") // see https://nodejs.org/api/http.html

// create a server and start listening
const server = http.createServer(requestListener)
server.listen(8081)

// requestListener is a function automatically added to each request
function requestListener(request, response) {
  response.write("hi")  // write stuff to the response
  response.end()        // finish preparing the response
}
console.log('Server running at http://127.0.0.1:8081/')

// How do we see our app?  What will appear?
